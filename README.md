# my-tv-apk

## 简介
my-tv应用用于自动升级时的应用存储

## 沟通平台
最新发版信息，不一定用my-tv-apk仓，my-tv-apk用于release发版（没有大的使用问题，不更新），中间的alpha、beta版本在修复最新的问题反馈（更新频繁），问题反馈沟通关注👉[技术论坛](https://www.zeppos.tech/t/topic/2821?u=sober)

## 最新发版下载
进入👉[技术论坛](https://www.zeppos.tech/t/topic/2821?u=sober)，根据图片教程，下载最新发版应用

![](res/%E6%9C%80%E6%96%B0%E5%8F%91%E7%89%88%E4%B8%8B%E8%BD%BD.png)